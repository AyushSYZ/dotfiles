    if has('termguicolors')
      set termguicolors
    endif

    let g:sonokai_style = 'andromeda'
    let g:sonokai_better_performance = 1

    colorscheme sonokai
    let g:lightline = {'colorscheme' : 'sonokai'}

