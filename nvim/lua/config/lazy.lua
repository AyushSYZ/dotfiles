--------------------------------------------------------------------------------
--------------------------------- lazy nvim ------------------------------------
--------------------------------------------------------------------------------

-- bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- plugins
require("lazy").setup({

    {
	    "nyoom-engineering/oxocarbon.nvim"
    } ,

    {
	    "nvim-treesitter/nvim-treesitter"
    } ,

	{
		'akinsho/toggleterm.nvim', version = "*", config = true
	} ,

    {
        'chipsenkbeil/distant.nvim', 
        branch = 'v0.3',
        config = function()
            require('distant'):setup()
        end
    }  ,

    {
    "beauwilliams/statusline.lua" ,
    dependencies = {'kyazdani42/nvim-web-devicons'}
    } ,

    {
       'nvim-telescope/telescope.nvim', tag = '0.1.5',
         dependencies = { 'nvim-lua/plenary.nvim' }
    } ,


})
