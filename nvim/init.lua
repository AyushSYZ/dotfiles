-- importing plugins
require("config.lazy")
require("config.options")
require("config.keymaps")
require("plugins")

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true


